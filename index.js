const _ = require('lodash');
const AWS = require('aws-sdk');
const csvreader = require('csv-parse');
const Storage = require('@openreply/storage');
const StorageS3Provider = require('@openreply/storage-s3');
const moment = require('moment');
const pEachSeries = require('p-each-series');
const Joi = require('@hapi/joi');

const config = {
    storage: {
        type: 'S3Storage',
        config: {
            bucket: process.env.STORAGE_CONFIG_S3_BUCKET,
            path: process.env.STORAGE_CONFIG_PATH,
            host: 'https://s3.eu-central-1.amazonaws.com'
        }
    },
    dynamodb: {
        tablename: process.env.DYNAMODB_TABLENAME
    }
};

// aws config and init
process.env.AWS_REGION && AWS.config.update({ region: process.env.AWS_REGION });
const dynamoDb = new AWS.DynamoDB();
const documentClient = new AWS.DynamoDB.DocumentClient();

Storage.registerProvider(StorageS3Provider);
const StorageClient = Storage.createClient({
    provider: config.storage.type,
    options: config.storage.config
});

const csvSchema = Joi.object({
    // we use .raw() to make sure, we get a validated string as the return value (as aws only accepts strings as input)
    DescriptionS3: Joi.string().raw(),
    Country: Joi.string().min(2).max(3).required()
        .raw(),
    CampaignId: Joi.string().required().raw(),
    Discount: Joi.number().required().raw(),
    RedeemRestriction: Joi.number().min(0).max(1).required()
        .raw(),
    RedeemRestrictionDescription: Joi.string().raw(),
    CustomercardRequired: Joi.string().raw(),
    RestrictionValue: Joi.number().raw(),
    ParticipatingStores: Joi.string().required().raw(),
    ValidFrom: Joi.string().required().raw(),
    ValidUntil: Joi.string().required().raw(),
    S3CouponId: Joi.string().required().raw() // TODO: is specified as number, but is more likely a string when stored in dynamoDb
});

const itemize = (couponData) => {
    if (couponData.length !== 12) {
        console.error(`Parsed entity has invalid length (${couponData.length} != 12)`);
        return;
    }
    // date validation, as German date format is not recognized by the joi.data module
    let validFrom;
    let validUntil;
    try {
        validFrom = moment(couponData[9], 'DD.MM.YYYY').toISOString();
        validUntil = moment(couponData[10], 'DD.MM.YYYY').toISOString();
    } catch (err) {
        console.log(err);
        return;
    }

    const schemaValidation = csvSchema.validate({
        DescriptionS3: couponData[0],
        Country: couponData[1],
        CampaignId: couponData[2],
        Discount: couponData[3],
        RedeemRestriction: couponData[4],
        RedeemRestrictionDescription: couponData[5],
        CustomercardRequired: couponData[6],
        RestrictionValue: couponData[7] || '0',
        ParticipatingStores: couponData[8],
        ValidFrom: validFrom,
        ValidUntil: validUntil,
        S3CouponId: couponData[11].toString()
    });

    if (schemaValidation.error) {
        console.error(schemaValidation); // log error to cloud watch, continue silently
        return null;
    }
    return {
        TableName: config.dynamodb.tablename,
        Item: {
            DescriptionS3: { S: schemaValidation.value.DescriptionS3 },
            Country: { S: schemaValidation.value.Country },
            CampaignId: { S: schemaValidation.value.CampaignId },
            Discount: { N: schemaValidation.value.Discount },
            RedeemRestriction: { N: schemaValidation.value.RedeemRestriction },
            RedeemRestrictionDescription: { S: schemaValidation.value.RedeemRestrictionDescription },
            CustomercardRequired: { S: schemaValidation.value.CustomercardRequired },
            RestrictionValue: { N: schemaValidation.value.RestrictionValue },
            ParticipatingStores: { SS: schemaValidation.value.ParticipatingStores.split(',') },
            ValidFrom: { S: schemaValidation.value.ValidFrom },
            ValidUntil: { S: schemaValidation.value.ValidUntil },
            S3CouponId: { S: schemaValidation.value.S3CouponId }
        }
    };
};

const diffEntries = (ddbEntries, s3Entries) => {
    // map the s3CouponIds as arrays for easier handling
    const s3CouponIdDdb = ddbEntries.map((a) => a.S3CouponId);
    const s3CouponIdS3 = s3Entries.map((a) => a.Item.S3CouponId.S);
    const updatedItemsIntersection = _.intersection(s3CouponIdDdb, s3CouponIdS3)
        .map((iid) => _.find(s3Entries, (o) => o.Item.S3CouponId.S === iid));
    const updatedItems = _.union(s3Entries, updatedItemsIntersection);

    const deletedItems = _.difference(s3CouponIdDdb, s3CouponIdS3)
        .map((iid) => ({
            TableName: config.dynamodb.tablename,
            Key: {
                S3CouponId: { S: iid }
            }
        }));
    return { updatedItems, deletedItems };
};


const getCurrentDdbEntries = async () => await documentClient.scan({
    TableName: config.dynamodb.tablename
}).promise();

const updateDdbEntries = async (updatedItems, deletedItems) => {
    try {
        await pEachSeries(updatedItems, async (updatedItem) => {
            console.log('Updating', JSON.stringify(updatedItem, null, 2));
            await dynamoDb.putItem(updatedItem).promise();
            console.log('Updated/Put data for ', updatedItem.Item.S3CouponId.S);
        });
        await pEachSeries(deletedItems, async (deletedItem) => {
            console.log('Deleting', JSON.stringify(deletedItem, null, 2));
            await dynamoDb.deleteItem(deletedItem).promise();
            console.log('Deleted', deletedItem, 'on dynamoDb table', config.dynamodb.tablename);
        });
    } catch (err) {
        console.log('Error', err);
        throw err;
    }
};

exports.handler = async (event) => new Promise(async (resolve, reject) => {
    try {
        console.log('Starting import', JSON.stringify(config, null, 2));
        const currentEntriesOnDdb = [];
        try {
            const data = await getCurrentDdbEntries();
            data.Items.forEach((item) => { currentEntriesOnDdb.push(item); });
        } catch (err) {
            console.error('Unable to query dynamoDb. Error:', JSON.stringify(err, null, 2));
            throw err;
        }
        console.log('currentEntriesOnDdb', JSON.stringify(currentEntriesOnDdb, null, 2));

        const itemizedS3Entries = [];
        StorageClient.createReadStream({ filePath: config.storage.config.path })
            .pipe(csvreader({
                delimiter: ';',
                skip_empty_lines: true,
                from: 2 // starts from 1, skip first, as it contains the field names
            }))
            .on('data', (row) => {
                console.log('csv row', row);
                itemizedS3Entries.push(itemize(row));
            })
            .on('end', async () => {
                console.log('itemizedS3Entries', JSON.stringify(itemizedS3Entries, null, 2));
                if (itemizedS3Entries) {
                    // diff the entries so find out, which coupons are no longer available
                    const { updatedItems, deletedItems } = diffEntries(currentEntriesOnDdb, itemizedS3Entries.filter((x) => x));
                    await updateDdbEntries(updatedItems, deletedItems);
                }
                console.log('Finished import');
                return resolve();
            });
    } catch (error) {
        return reject(error);
    }
});
