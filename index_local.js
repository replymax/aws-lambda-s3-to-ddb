const _ = require('lodash');
const AWS = require('aws-sdk');
const csvreader = require('csv-parse');
const Storage = require('@openreply/storage');
const StorageS3Provider = require('@openreply/storage-s3');
const moment = require('moment');
const Joi = require('@hapi/joi');

const config = {
    storage: {
        type: 'S3Storage',
        config: {
            bucket: process.env.STORAGE_CONFIG_S3_BUCKET,
            path: process.env.STORAGE_CONFIG_PATH,
            host: 'https://s3.eu-central-1.amazonaws.com'
        }
    },
    dynamodb: {
        tablename: process.env.DYNAMODB_TABLENAME
    },
    awsregion: process.env.AWS_REGION
};

// aws config and init
AWS.config.update({ region: config.awsregion });
const dynamoDb = new AWS.DynamoDB();
const documentClient = new AWS.DynamoDB.DocumentClient();

Storage.registerProvider(StorageS3Provider);
const StorageClient = Storage.createClient({
    provider: config.storage.type,
    options: config.storage.config
});

const csvSchema = Joi.object({
    // we use .raw() to make sure, we get a validated string as the return value (as aws only accepts strings as input)
    descriptionS3: Joi.string().raw(),
    country: Joi.string().min(2).max(3).required().raw(),
    campaignId: Joi.string().required().raw(),
    discount: Joi.number().required().raw(),
    redeemRestriction: Joi.number().min(0).max(1).required().raw(),
    redeemRestrictionDescription: Joi.string().raw(),
    customercardRequired: Joi.string().raw(),
    restrictionValue: Joi.number().raw(),
    participatingStores: Joi.string().required().raw(),
    validFrom: Joi.string().required().raw(),
    validUntil: Joi.string().required().raw(),
    s3CouponId: Joi.string().required().raw() // TODO: is specified as number, but is more likely a string when stored in dynamoDb
});

const itemize = (couponData) => {
    if (couponData.length != 12) {
        console.error(`Parsed entity has invalid length (${couponData.length} != 12)`);
        return;
    }
    // date validation, as German date format is not recognized by the joi.data module
    let validFrom;
    let validUntil;
    try {
        validFrom = moment(couponData[9], 'DD.MM.YYYY').toISOString();
        validUntil = moment(couponData[10], 'DD.MM.YYYY').toISOString();
    } catch (err) {
        console.log(err);
        return;
    }

    const schemaValidation = csvSchema.validate({
        descriptionS3: couponData[0],
        country: couponData[1],
        campaignId: couponData[2],
        discount: couponData[3],
        redeemRestriction: couponData[4],
        redeemRestrictionDescription: couponData[5],
        customercardRequired: couponData[6],
        restrictionValue: couponData[7] || '0',
        participatingStores: couponData[8],
        validFrom,
        validUntil,
        s3CouponId: couponData[11]
    });

    if (schemaValidation.error) {
        console.error(schemaValidation); // log error to cloud watch, continue silently
    } else {
        return {
            TableName: config.dynamodb.tablename,
            Item: {
                descriptionS3: { S: schemaValidation.value.descriptionS3 },
                country: { S: schemaValidation.value.country },
                campaignId: { S: schemaValidation.value.campaignId },
                discount: { N: schemaValidation.value.discount },
                redeemRestriction: { N: schemaValidation.value.redeemRestriction },
                redeemRestrictionDescription: { S: schemaValidation.value.redeemRestrictionDescription },
                customercardRequired: { S: schemaValidation.value.customercardRequired },
                restrictionValue: { N: schemaValidation.value.restrictionValue },
                participatingStores: { S: schemaValidation.value.participatingStores },
                validFrom: { S: schemaValidation.value.validFrom },
                validUntil: { S: schemaValidation.value.validUntil },
                s3CouponId: { S: schemaValidation.value.s3CouponId }
            }
        };
    }
};

const diffEntries = (ddbEntries, s3Entries) => {
    // map the s3CouponIds as arrays for easier handling
    const s3CouponIdDdb = ddbEntries.map((a) => a.s3CouponId);
    const s3CouponIdS3 = s3Entries.map((a) => a.Item.s3CouponId.S);
    const updatedItemsIntersection = _.intersection(s3CouponIdDdb, s3CouponIdS3)
        .map((iid) => _.find(s3Entries, (o) => o.Item.s3CouponId.S === iid));
    const updatedItems = _.union(s3Entries, updatedItemsIntersection);

    const deletedItems = _.difference(s3CouponIdDdb, s3CouponIdS3)
        .map((iid) => ({
            TableName: config.dynamodb.tablename,
            Key: {
                s3CouponId: { S: iid }
            }
        }));
    return { updatedItems, deletedItems };
};


const getCurrentDdbEntries = async () => await documentClient.scan({
    TableName: config.dynamodb.tablename
}, (err, data) => {
    if (err) {
        console.error('Unable to query dynamoDb. Error:', JSON.stringify(err, null, 2));
    }
}).promise();

const updateDdbEntries = (updatedItems, deletedItems) => {
    updatedItems.forEach((updatedItem) => {
        dynamoDb.putItem(updatedItem, (err, data) => {
            err ? console.log('Error', err) : console.log('Updated/Put data for ', updatedItem.Item.s3CouponId.S);
        });
    });
    deletedItems.forEach((deletedItem) => {
        dynamoDb.deleteItem(deletedItem, (err, data) => {
            err ? console.log('Error', err) : console.log('Deleted', deletedItem, 'on dynamoDb table', config.dynamodb.tablename);
        });
    });
};

// exports.handler = async (event) => {
_handler = async (event) => {
    const currentEntriesOnDdb = [];
    await getCurrentDdbEntries().then((data) => {
        data.Items.forEach((item) => { currentEntriesOnDdb.push(item); });
    });

    const itemizedS3Entries = [];
    StorageClient.createReadStream({ filePath: config.storage.config.path })
        .pipe(csvreader({
            delimiter: ';',
            skip_empty_lines: true,
            from: 2 // starts from 1, skip first, as it contains the field names
        }))
        .on('data', (row) => {
            itemizedS3Entries.push(itemize(row));
        })
        .on('end', () => {
            if (itemizedS3Entries) {
                // diff the entries so find out, which coupons are no longer available
                const { updatedItems, deletedItems } = diffEntries(currentEntriesOnDdb, itemizedS3Entries);
                updateDdbEntries(updatedItems, deletedItems);
            }
        });
};

_handler();
