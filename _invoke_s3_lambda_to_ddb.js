const AWS = require('aws-sdk');
AWS.config.update({ region: 'eu-central-1' });
const lambda = new AWS.Lambda({ apiVersion: '2015-03-31' });


const invokeS3toDdbLambda = async () => new Promise((resolve, reject) => {
    const params = {
        FunctionName: "lambda-s3-to-ddb-test",
        InvocationType: 'RequestResponse',
        LogType: 'None'
    };
    lambda.invoke(params, function (err, data) {
        err ? console.log("Error", err):console.log("Success", data);
    });
});

exports.handler = async (event, callback) => {
    /**
     * remaining code...
     */

    let invokeResult;
    try {
        invokeResult = await invokeS3toDdbLambda();
        invokeResult = JSON.parse(invokeResult.toString());
        invokeResult = invokeResult.filter(elem => elem);
    } catch (err) {
        console.error("Error while getting audit from API: ", err);
        console.error("response: ", invokeResult);
        return;
    }
};


